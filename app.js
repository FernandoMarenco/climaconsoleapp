const lugar = require('./lugar/lugar');
const clima = require('./clima/clima');

const argv = require('yargs')
    .options({
        direccion: {
            alias: 'd',
            desc: 'Dirección para obtener el clima',
            demand: true
        }
    })
    .argv;

//console.log(argv.direccion);

// lugar.getLugarLatLng(argv.direccion)
//     .then(resp => {
//         console.log(resp);
//     });

// clima.getClima(40.750000, -74.000000)
//     .then(resp => {
//         console.log(resp)
//     })
//     .catch(err => {
//         console.log(err)
//     });

const getInfo = async(direccion) => {

    try {
        const l = await lugar.getLugarLatLng(direccion);
        const c = await clima.getClima(l.lat, l.lng);

        return `El clima de ${l.direccion} es de ${c.temp}`;

    } catch (e) {
        return `No se pudo encontrar el clima para: ${direccion}. \n${e}`;

    }

}

getInfo(argv.direccion)
    .then(resp => console.log(resp))
    .catch(err => console.log(err));