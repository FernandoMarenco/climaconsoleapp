const axios = require('axios');
const { APIKEY_OPENWEATHERMAP } = require('../secrets/api-keys');

const getClima = async(lat, lng) => {

    let instace = axios.create({
        baseURL: `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&APPID=${APIKEY_OPENWEATHERMAP}&lang=es&units=metric`
    });

    const resp = await instace.get();

    return resp.data.main

}

module.exports = {
    getClima
}