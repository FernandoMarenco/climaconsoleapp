const axios = require('axios');
const { APIKEY_RAPIDAPI } = require('../secrets/api-keys');

const getLugarLatLng = async(dir) => {

    let locationEncode = encodeURI(dir);
    //console.log(locationEncode);

    let instance = axios.create({
        baseURL: `https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location=${locationEncode}`,
        headers: {
            'X-RapidAPI-Key': APIKEY_RAPIDAPI
        }
    });

    const resp = await instance.get();

    if (resp.data.Results.length === 0) {
        throw new Error(`No se encontraron resultados para ${dir}`)
    }

    const data = resp.data.Results[0];
    const direccion = data.name;
    const lat = data.lat;
    const lng = data.lon;

    return {
        direccion,
        lat,
        lng
    }
}

module.exports = {
    getLugarLatLng
}