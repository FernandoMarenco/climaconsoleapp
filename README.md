# Clima console app

Esta es una aplicación donde se utiliza la consola para consultar el clima de un país/ciudad.

Comando: 
```--direccion, -d='nombre de la dirección'``` => para obtener el clima de la dirección escrita


## Ejemplo

Obtener el clima de Vancouver:

```
node app -d "Vancouver"
```


```npm install```
Para instalar los node_modules